#![no_std]
use arrayvec::ArrayString;

use core;
use core::ffi::c_char;

use core::fmt::Write;

extern "C" {
    pub fn putchar(arg: c_char);
}

fn write_out(s: &str) {
    for c in s.as_bytes() {
        unsafe { putchar(*c as c_char) };
    }
}

pub fn run() {
    let mut data = ArrayString::<32>::new();
    
    for i in (0..3).rev() {
        let _ = writeln!(data, "{} days left", i);
        write_out(data.as_str());
        data.clear();
    }
    write_out("camp2023\n");
}
