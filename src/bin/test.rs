use emc;
use std::fmt::Write;

fn main() {
    let mut data = String::new();
    
    for i in (0..3).rev() {
        let _ = writeln!(&mut data, "{} days left", i);
        print!("{}", &data);
        data.clear();
    }
    emc::run();
}